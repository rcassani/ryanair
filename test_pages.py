from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from test_helper import *

class Page(object):

	def __init__(self, test):
		self._test = test
		self._driver = test.driver

	def assertUrl(self, expected_url): #we wait until the condition expressed by the runtime function is met
		self._test.assertTrue(WebDriverWait(self._driver, DEFAULT_WAIT).until(
			lambda s: s.current_url == expected_url, 
			"The URL of the page: %s doesn't match the expected one: %s." % (self._driver.current_url, expected_url)))
	
	def removeCookiePopup(self):
		try:
			element = WebDriverWait(self._driver, DEFAULT_WAIT).until(
				EC.presence_of_element_located((By.XPATH, "//div[@class='cookie-popup']/core-icon")))
			if element:
				element.click()
		except:
			pass
		
class HomePage(Page):

	def __init__(self, test):
		super(HomePage, self).__init__(test)
		self.assertUrl("https://www.ryanair.com/ie/en/")
	
	def pageTitleIsCorrect(self):
		self._test.assertEqual(self._driver.title, "Official Ryanair website | Cheap flights | Exclusive deals")
	
	@property
	def oneWayTripRadioButton(self):
		by = (By.ID, "flight-search-type-option-one-way")
		return RadioButton( self._driver, by, 'one-way')
			
	@property
	def returnTripRadioButton(self):
		by = (By.ID, "flight-search-type-option-return")
		return RadioButton( self._driver, by, "return trip")
	
	@property
	def origin(self):
		by = (By.XPATH, "(//input[@type='text'])[2]")
		return InputTextBox( self._driver, by, "origin airport")
	
	@property
	def destination(self):
		by = (By.XPATH, "(//input[@type='text'])[4]")
		return InputTextBox( self._driver, by, "destination airport")
	
	@property
	def continueButton(self):
		by = (By.XPATH, "//div[@id='search-container']/div/div/form/div[3]/button")
		return ClickableElement( self._driver, by, "continue button")
	
	@property
	def dateField(self):
		by = (By.XPATH, "//div[@id='row-dates-pax']/div/div/div/div/div[2]/div[2]/div/div")
		return ClickableElement( self._driver, by, "date field")
	
	def dateSelect(self, date):
		element = WebDriverWait(self._driver, DEFAULT_WAIT).until(
			EC.element_to_be_clickable((By.XPATH, "//li[@data-id='" + date + "']")), "The date could not be selected.")
		element.click()
	
	@property
	def passengersSelect(self):
		by = (By.CSS_SELECTOR, "div.dropdown-handle > core-icon.chevron.ng-isolate-scope > div > svg")
		return ClickableElement( self._driver, by, "passenger selection")
	
	def addAdult(self):
		element = WebDriverWait(self._driver, DEFAULT_WAIT).until(
			EC.presence_of_element_located((By.XPATH, "//div[@label='Adults']//button[2]")), "The adult could not be added.")
		element.click()
	
	@property
	def letsGoLink(self):
		by = (By.XPATH, "//div[@id='search-container']/div/div/form/div[3]/button[2]")
		return Link(self._driver, by, "let's go", FlightSelection, self._test)
	
class FlightSelection(Page):

	def __init__(self, test):
		super(FlightSelection, self).__init__(test)
		self.assertUrl("https://www.ryanair.com/ie/en/booking/home")

	@property
	def lowestFare(self):
		by = (By.XPATH, "//div[@class='flight-prices']/div[2]/div")
		return ClickableElement( self._driver, by, "lowest fare")
	
	@property
	def continueButton(self):
		by = (By.XPATH, "//button[@type='button' and contains(., 'Continue')]")
		return Link( self._driver, by, "continue button", BookingExtras, self._test)
		
		

class BookingExtras(Page):

	def __init__(self, test):
		super(BookingExtras, self).__init__(test)
		self.assertUrl("https://www.ryanair.com/ie/en/booking/extras")
		
		try: #take care of the popup
			element = WebDriverWait(self._driver, DEFAULT_WAIT).until(
				EC.element_to_be_clickable((By.XPATH, "//div[@class='dialog-title-right']/core-icon/div")))
			if element:
				element.click()
		except:
			pass
		
	@property
	def checkoutButton(self):
		by = (By.XPATH, "(//button[@type='button' and contains(., 'Check out')])[2]")
		return Link( self._driver, by, "checkout button", BookingPayment, self._test)

class BookingPayment(Page):

	def __init__(self, test):
		super(BookingPayment, self).__init__(test)
		self.assertUrl("https://www.ryanair.com/ie/en/booking/payment")
		
	@property
	def adult1Title(self):
		by = (By.XPATH, "//select[starts-with(@id, 'title')]")
		return SelectField( self._driver, by, "adult 1's title")
		
	@property
	def adult1FirstName(self):
		by = (By.XPATH, "//input[starts-with(@id, 'firstName')]")
		return InputTextBox( self._driver, by, "adult 1's first name")
	
	@property
	def adult1LastName(self):
		by = (By.XPATH, "//input[starts-with(@id, 'lastName')]")
		return InputTextBox( self._driver, by, "adult 1's last name")
		
	@property
	def adult2Title(self):
		by = (By.XPATH, "(//select[starts-with(@id, 'title')])[2]")
		return SelectField( self._driver, by, "adult 2's title")
		
	@property
	def adult2FirstName(self):
		by = (By.XPATH, "(//input[starts-with(@id, 'firstName')])[2]")
		return InputTextBox( self._driver, by, "adult 2's first name")
	
	@property
	def adult2LastName(self):
		by = (By.XPATH, "(//input[starts-with(@id, 'lastName')])[2]")
		return InputTextBox( self._driver, by, "adult 2's last name")
	
	@property
	def emailAddress(self):
		by = (By.XPATH, "//input[starts-with(@id, 'emailAddress')]")
		return InputTextBox( self._driver, by, "email address")
	
	@property
	def phoneCountry(self):
		by = (By.XPATH, "//select[@name='phoneNumberCountry']")
		return SelectField( self._driver, by, "phone country")
	
	@property
	def phoneNumber(self):
		by = (By.XPATH, "//input[@name='phoneNumber']")
		return InputTextBox( self._driver, by, "phone number")
	
	@property
	def smsCheck(self):
		by = (By.XPATH, "//input[@name='phoneNumber']")
		return Checkbox( self._driver, by, False, "sms")
	
	@property
	def cardNumber(self):
		by = (By.XPATH, "//input[starts-with(@id, 'cardNumber')]")
		return InputTextBox( self._driver, by, "card number")
		
	@property
	def cardType(self):
		by = (By.XPATH, "//select[starts-with(@id, 'cardType')]")
		return SelectField( self._driver, by, "card type")
		
	@property
	def expiryMonth(self):
		by = (By.XPATH, "//select[starts-with(@id, 'expiryMonth')]")
		return SelectField( self._driver, by, "expiry month")	
		
	@property
	def expiryYear(self):
		by = (By.XPATH, "//select[@name='expiryYear']")
		return SelectField( self._driver, by, "expiry year")	

	@property
	def securityCode(self):
		by = (By.XPATH, "//input[@name='securityCode']")
		return InputTextBox( self._driver, by, "cvv")
	
	@property
	def cardHolder(self):
		by = (By.XPATH, "//input[@name='cardHolderName']")
		return InputTextBox( self._driver, by, "card holder")
	
	@property
	def addressLine1(self):
		by = (By.ID, "billingAddressAddressLine1")
		return InputTextBox( self._driver, by, "address line 1")
	
	@property
	def addressLine2(self):
		by = (By.ID, "billingAddressAddressLine2")
		return InputTextBox( self._driver, by, "address line 2")
	
	@property
	def addressCity(self):
		by = (By.ID, "billingAddressCity")
		return InputTextBox( self._driver, by, "address city")
	
	@property
	def postCode(self):
		by = (By.ID, "billingAddressPostcode")
		return InputTextBox( self._driver, by, "post code")
	
	@property
	def addressCountry(self):
		by = (By.ID, "billingAddressCountry")
		return SelectField( self._driver, by, "address country")
	
	@property
	def acceptTerms(self):
		by = (By.XPATH, "//input[starts-with(@id, 'acceptTerms')]")
		return Checkbox( self._driver, by, False, "accept terms")
	
	@property
	def submitButton(self):
		by = (By.XPATH, "//button[@type='submit']")
		return Link( self._driver, by, "submit button", BookingPayment, self._test)
	
	def unsuccesfulPaymentMessageIsShown(self):
		self._test.assertTrue(WebDriverWait(self._driver, DEFAULT_WAIT).until(
			EC.text_to_be_present_in_element((By.XPATH, "//div[@class='payment-form']/div/prompt/div[1]"), "Oh. There was a problem"),
												"The unsuccesful payment message wasn't found."))		

		
class SelectField(object):
	def __init__(self, driver, by, name):
		self._se = WebDriverWait(driver, DEFAULT_WAIT).until( EC.element_to_be_clickable(by), 
												"The " + name + " select wasn't found.")
	def select(self, text):
		Select(self._se).select_by_visible_text(text)

class Checkbox(object):
	def __init__(self, driver, by, set, name):
		if WebDriverWait(driver, DEFAULT_WAIT).until( EC.element_located_selection_state_to_be(by, set), 
												"The " + name + " checkbox  wasn't found."):
			_by, _path = by
			self._cb = driver.find_element(_by, _path)
			self._set = set
	
	def setTrue(self):
		if not self._set:
			self._cb.click()
			self._set = not self._set
			
	def setFalse(self):
		if self._set:
			self._cb.click()
			self._set = not self._set
		
class RadioButton(object):
	def __init__(self, driver, by, name):
		self._rb = WebDriverWait(driver, DEFAULT_WAIT).until( EC.presence_of_element_located(by), 
												"The " + name + " radio button wasn't found.")
	def select(self):
		self._rb.click()

class InputTextBox(object):
	def __init__(self, driver, by, name):
		self._tb = WebDriverWait(driver, DEFAULT_WAIT).until( EC.element_to_be_clickable(by), 
												"The " + name + " text box wasn't ready to be clicked.")
	def fillIn(self, text):
		self._tb.click()
		self._tb.clear()
		self._tb.send_keys(text)

class ClickableElement(object):
	def __init__(self, driver, by, name):
		self._ce = WebDriverWait(driver, DEFAULT_WAIT).until( EC.element_to_be_clickable(by), 
												"The " + name + " element wasn't ready to be clicked.")
	def click(self):
		self._ce.click()
		
class Link(ClickableElement):
	def __init__(self, driver, by, name, dest_page, test):
		super( Link, self).__init__( driver, by, name)
		self._dest_page = dest_page
		self._test = test
		
	def click(self):
		super( Link, self).click()
		return self._dest_page(self._test)
