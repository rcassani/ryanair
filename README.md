# README #

In order to run the test some tools/components are needed.

1. Install Python 2.7.x from www.python.org/downloads/ and choose during installation to add python binaries to the PATH
2. Run: source ./env/Scripts/activate
3. Run: python ryan_test.py

This simple Python test framework makes use of the unittest and selenium libraries to achieve the result. The selenium webdriver is set-up towards Firefox.