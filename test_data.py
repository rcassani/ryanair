class TestData():

	class OneWayTrip():
		origin = 'Wroclaw'
		destination = 'Bologna'
		date = '25-05-2016'

	class People():

		class Adult1():
			title = 'Mr'
			first_name = 'Riccardo'
			last_name = 'Cassani'

		class Adult2():
			title = 'Ms'
			first_name = 'Magdalena'
			last_name = 'Zawadzka'

	class ContactInfo():
		email = 'cassanir@ryanair.com'
		phone_country = 'Italy'
		phone_number = '5437891235'

	class CardInfo():
		number = '5555555555555557'
		type = 'MasterCard'
		exp_month = '10'
		exp_year = '2018'
		cvv = '123'
		card_holder = 'Riccardo Cassani'

	class BillingAddress():
		line1 = 'Palace Street 4'
		line2 = ''
		city = 'Tin Town'
		post_code = '40021'
		country = 'Italy'
