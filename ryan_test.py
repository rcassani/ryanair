from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import unittest, time, re

from test_data import TestData as TD
from test_helper import *
from test_pages import HomePage, FlightSelection, BookingExtras, BookingPayment

class RyanTest(unittest.TestCase):
	def setUp(self):
		self.driver = eval('webdriver.' + TARGET_BROWSER)()
		self.driver.implicitly_wait(DEFAULT_WAIT)
		self.base_url = "https://www.ryanair.com"
		self.verificationErrors = []
		self.accept_next_alert = False
	
	def test_title(self):
		self.driver.get(self.base_url + "/ie/en")
		page = HomePage(self)
		page.pageTitleIsCorrect()
	
	def test_booking_one_way_for_2_adults(self):
		driver = self.driver
		#setup home page
		self.driver.get(self.base_url + "/ie/en")
		home = HomePage(self)
		home.removeCookiePopup()
		
		#search trip
		home.oneWayTripRadioButton.select()
		home.destination.fillIn(TD.OneWayTrip.destination)
		home.origin.fillIn(TD.OneWayTrip.origin)
		home.continueButton.click()
		home.dateField.click()
		home.dateSelect(TD.OneWayTrip.date)
		home.passengersSelect.click()
		home.addAdult()
		flight_selection = home.letsGoLink.click()
		
		#next page
		flight_selection.lowestFare.click()
		booking_extras = flight_selection.continueButton.click()
		
		#next page
		payment = booking_extras.checkoutButton.click()

		#next page
		#passenger info
		payment.adult1Title.select(TD.People.Adult1.title)
		payment.adult1FirstName.fillIn(TD.People.Adult1.first_name)
		payment.adult1LastName.fillIn(TD.People.Adult1.last_name)
		payment.adult2Title.select(TD.People.Adult2.title)
		payment.adult2FirstName.fillIn(TD.People.Adult2.first_name)
		payment.adult2LastName.fillIn(TD.People.Adult2.last_name)
		#contactDetails
		payment.emailAddress.fillIn(TD.ContactInfo.email)
		payment.phoneCountry.select(TD.ContactInfo.phone_country)
		payment.phoneNumber.fillIn(TD.ContactInfo.phone_number)
		payment.smsCheck.setTrue()
		#cardDetails
		payment.cardNumber.fillIn(TD.CardInfo.number)
		payment.cardType.select(TD.CardInfo.type)
		payment.expiryMonth.select(TD.CardInfo.exp_month)
		payment.expiryYear.select(TD.CardInfo.exp_year)
		payment.securityCode.fillIn(TD.CardInfo.cvv)
		payment.cardHolder.fillIn(TD.CardInfo.card_holder)
		#billingAddress
		payment.addressLine1.fillIn(TD.BillingAddress.line1)
		payment.addressLine2.fillIn(TD.BillingAddress.line2)
		payment.addressCity.fillIn(TD.BillingAddress.city)
		payment.postCode.fillIn(TD.BillingAddress.post_code)
		payment.addressCountry.select(TD.BillingAddress.country)
		#acceptTerms and submit
		payment.acceptTerms.setTrue()
		payment2 = payment.submitButton.click()		
		payment2.unsuccesfulPaymentMessageIsShown()
		

	def is_element_present(self, how, what):
		try: self.driver.find_element(by=how, value=what)
		except NoSuchElementException as e: return False
		return True

	def is_alert_present(self):
		try: self.driver.switch_to_alert()
		except NoAlertPresentException as e: return False
		return True

	def close_alert_and_get_its_text(self):
		try:
			alert = self.driver.switch_to_alert()
			alert_text = alert.text
			if self.accept_next_alert:
				alert.accept()
			else:
				alert.dismiss()
			return alert_text
		finally: self.accept_next_alert = True
    
	def tearDown(self):
		self.driver.quit()
		self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
	unittest.main()